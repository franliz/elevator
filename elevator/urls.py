from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('home/', include('elevator_app.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('', include('social.apps.django_app.urls', namespace='social')),
]
