from django.urls import path
from . import views

urlpatterns = [
    path('client_list/', views.client_list, name='client_list'),
    path('add_client/', views.add_client, name='add_client'),
    path('edit_client/<int:id>', views.edit_client, name='edit_client'),
    path('delete_client/<int:id>', views.delete_client, name='delete_client'),
    path('assign_tech/<int:id>', views.assign_tech, name='assign_tech'),
    path('create_odt/<int:id>', views.create_odt, name='create_odt'),
    path('odt_list/', views.odt_list, name='odt_list'),
    path('edit_odt/<int:id>', views.edit_odt, name='edit_odt'),
    path('add_detail/<int:id>', views.add_detail, name='add_detail'),
    path('tech_list/', views.tech_list, name='tech_list'),
    path('add_tech/', views.add_tech, name='add_tech'),
    path('edit_tech/<int:id>', views.edit_tech, name='edit_tech'),
    path('delete_tech/<int:id>', views.delete_tech, name='delete_tech'),
]
