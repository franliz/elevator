from django.shortcuts import render, redirect
from .models import Client, User, Elevator, Odt, Detail
from django.template.response import TemplateResponse
import datetime

def client_list(request):
    if request.user.is_authenticated:
        if request.user.is_staff:
            data = Client.objects.all()
            return TemplateResponse(request, 'elevator_app/adm_client_list.html', {"data": data})
        else:
            tecnico_autorizado = request.user
            data = Client.objects.filter(tech=tecnico_autorizado)
            return TemplateResponse(request, 'elevator_app/tech_client_list.html', {"data": data})
    else:
        return redirect('login')

def add_client(request):
    if request.method == 'POST':
        if (request.POST.get('name') and request.POST.get('address') and request.POST.get('region') and request.POST.get('city') and request.POST.get('phone') and request.POST.get('email')):
            cliente = Client()
            cliente.name = request.POST.get('name')
            cliente.address = request.POST.get('address')
            cliente.region = request.POST.get('region')
            cliente.city = request.POST.get('city')
            cliente.phone = request.POST.get('phone')
            cliente.email = request.POST.get('email')
            cliente.save()
            return redirect('client_list')
    else:
            return render(request, 'elevator_app/add_client.html')
        
def edit_client(request, id):
    if request.method == 'GET':
        cliente_id = id
        cliente = Client.objects.get(id=cliente_id)
        if not cliente_id:
            return redirect('client_list')
        else:
            return TemplateResponse(request, 'elevator_app/edit_client.html', {"cliente": cliente})
    else:
        if (request.POST.get('name') and request.POST.get('address') and request.POST.get('region') and request.POST.get('city') and request.POST.get('phone') and request.POST.get('email')):
            cliente_id = id
            cliente = Client.objects.get(id=cliente_id)
            cliente.name = request.POST.get('name')
            cliente.address = request.POST.get('address')
            cliente.region = request.POST.get('region')
            cliente.city = request.POST.get('city')
            cliente.phone = request.POST.get('phone')
            cliente.email = request.POST.get('email')
            cliente.save()
            return redirect('client_list')
            
def delete_client(request, id):
    if request.method == 'GET':
        cliente_id = id
        if cliente_id:
            cliente = Client.objects.get(id=cliente_id)
            cliente.delete()
    return redirect('client_list')

def assign_tech(request, id):
    if request.method == 'GET':
        tecnicos = User.objects.all()
        cliente_id = id
        cliente = Client.objects.get(id=cliente_id)
        if not cliente_id:
            return redirect('client_list')
        else:
            return TemplateResponse(request, 'elevator_app/assign_tech.html', {"cliente": cliente, "tecnicos": tecnicos})
    else:
        cliente_id = id
        cliente = Client.objects.get(id=cliente_id)
        tecnico_id = request.POST.get('tech')
        tecnico_seleccionado = User.objects.get(id=tecnico_id)
        cliente.tech = tecnico_seleccionado
        cliente.save()
        return redirect('client_list')

def create_odt(request, id):
    if request.method == 'GET':
        if request.user.is_authenticated:
            if not request.user.is_staff:
                cliente_id = id
                if not cliente_id:
                    return redirect('client_list')
                else:
                    cliente = Client.objects.get(id=cliente_id)
                    tecnico = request.user
                    if cliente.tech == tecnico:
                        orden = Odt()
                        orden.client = cliente
                        orden.tech = tecnico
                        orden.start_date = datetime.datetime.today().strftime('%Y-%m-%d')
                        orden.start_time = datetime.datetime.today().strftime('%H:%M')
                        orden.save()
                        return redirect('odt_list')
            else:
                return redirect('client_list')
        else:
            return redirect('login')
    
def odt_list(request):
    if request.user.is_authenticated:
        if request.user.is_staff:
            data = Odt.objects.all()
            return TemplateResponse(request, 'elevator_app/adm_odt_list.html', {"data": data})
        else:
            tecnico_autorizado = request.user
            data = Odt.objects.filter(tech=tecnico_autorizado)
            return TemplateResponse(request, 'elevator_app/tech_odt_list.html', {"data": data})
    else:
        return redirect('login')

def edit_odt(request, id):
    if request.method == 'GET':
        orden_id = id
        orden = Odt.objects.get(id=orden_id)
        detalles = Detail.objects.filter(odt=orden)
        if not orden_id:
            return redirect('odt_list')
        else:
            return TemplateResponse(request, 'elevator_app/edit_odt.html', {"orden": orden, "detalles": detalles})
    else:
        orden_id = id
        orden = Odt.objects.get(id=orden_id)
        orden.end_date = datetime.datetime.today().strftime('%Y-%m-%d')
        orden.end_time = datetime.datetime.today().strftime('%H:%M')
        orden.save()
        return redirect('odt_list')

def add_detail(request, id):
    if request.method == 'GET':
        orden_id = id
        orden = Odt.objects.get(id=orden_id)
        ascensores = Elevator.objects.all()
        if not orden_id:
            return redirect('odt_list')
        else:
            return TemplateResponse(request, 'elevator_app/add_detail.html', {"orden": orden, "ascensores": ascensores})
    else:
        orden_id = id
        orden = Odt.objects.get(id=orden_id)
        if (request.POST.get('elevator') and request.POST.get('detail')):
            ascensor_id = request.POST.get('elevator')
            ascensor = Elevator.objects.get(id=ascensor_id)
            detalle = Detail()
            detalle.odt = orden
            detalle.elevator = ascensor
            detalle.detail = request.POST.get('detail')
            detalle.failure = request.POST.get('failure')
            detalle.piezas = request.POST.get('piezas')
            detalle.save()    
            return redirect('edit_odt', orden_id)

def tech_list(request):
    if request.user.is_authenticated:
        if request.user.is_staff:
            data = User.objects.exclude(is_staff=True)
            return TemplateResponse(request, 'elevator_app/adm_tech_list.html', {"data": data})
    else:
        return redirect('login')

def add_tech(request):
    if request.method == 'POST':
        if (request.POST.get('name') and request.POST.get('email') and request.POST.get('phone') and request.POST.get('password1') and request.POST.get('password2')):
            password1 = request.POST.get('password1')
            password2 = request.POST.get('password2')
            email = request.POST.get('email') 
            if password1 == password2:
                tecnico = User.objects.create_user(email, password1)
                tecnico.name = request.POST.get('name')
                tecnico.phone = request.POST.get('phone')
                tecnico.save()
                return redirect('tech_list')
    else:
            return render(request, 'elevator_app/add_tech.html')

def edit_tech(request, id):
    if request.method == 'GET':
        tecnico_id = id
        tecnico = User.objects.get(id=tecnico_id)
        if not tecnico_id:
            return redirect('tech_list')
        else:
            return TemplateResponse(request, 'elevator_app/edit_tech.html', {"tecnico": tecnico})
    else:
        if (request.POST.get('name') and request.POST.get('email') and request.POST.get('phone') and request.POST.get('password1') and request.POST.get('password2')):
            password1 = request.POST.get('password1')
            password2 = request.POST.get('password2')
            email = request.POST.get('email') 
            if password1 == password2:
                tecnico_id = id
                tecnico = User.objects.get(id=tecnico_id)
                tecnico.name = request.POST.get('name')
                tecnico.email = request.POST.get('email')
                tecnico.phone = request.POST.get('phone')
                tecnico.set_password(password1)
                tecnico.save()
                return redirect('tech_list')

def delete_tech(request, id):
    if request.method == 'GET':
        tecnico_id = id
        if tecnico_id:
            tecnico = User.objects.get(id=tecnico_id)
            tecnico.delete()
    return redirect('tech_list')

def delete_odt(request, id):
    if request.method == 'GET':
        orden_id = id
        if orden_id:
            orden = Odt.objects.get(id=orden_id)
            orden.delete()
    return redirect('odt_list')